# Project 9 (P9): Analyzing the Movies


## Corrections and clarifications:

* None yet

**Find any issues?** Create a post on Piazza

## NOTE:

Please go through [Lab 9](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-lecture-material/-/tree/main/sum23/labs/lab9) before you start the project. The lab contains some very important information that will be necessary for you to finish the project."

## Instructions:

This project will focus on **bucketizing**, **plotting** and **sorting**. To start, download [`p9.ipynb`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-lecture-material/-/blob/main/sum23/projects/p9/p9.ipynb), [`p9_test.py`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-lecture-material/-/blob/main/sum23/projects/p9/p9_test.py), [`movies.csv`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-lecture-material/-/blob/main/sum23/projects/p9/movies.csv), and [`mapping.csv`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-lecture-material/-/blob/main/sum23/projects/p9/mapping.csv).

If it takes too long to load the files `movies.csv` and `mapping.csv` on GitLab, you can directly download the file from these links: [`movies.csv`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-lecture-material/-/raw/main/sum23/projects/p9/movies.csv), and [`mapping.csv`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-lecture-material/-/raw/main/sum23/projects/p9/). You will need to **Right Click**, and click on the **Save as...** button to save the file through this method.

**Important:** Note that these files `movies.csv` and `mapping.csv` are the same files that you worked with in P8. So, you may just copy/paste the files from your P8 directory if you wish to.

You will work on `p9.ipynb` and hand it in. You should follow the provided directions for each question. Questions have **specific** directions on what **to do** and what **not to do**.

------------------------------

## IMPORTANT Submission instructions:
- Review the [Grading Rubric](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-lecture-material/-/blob/main/sum23/projects/p9/rubric.md), to ensure that you don't lose points during code review.
- You must **save your notebook file** before you run the cell containing **export**.
- Login to [Gradescope](https://www.gradescope.com/) and upload the zip file into the P9 assignment.
- If you completed the project with a **partner**, make sure to **add their name** by clicking "Add Group Member"
in Gradescope when uploading the p9 zip file.

   <img src="images/add_group_member.png" width="400">

   **Warning:** You will have to add your partner on Gradescope even if you have filled out this information in your `p9.ipynb` notebook.

- It is **your responsibility** to make sure that your project clears auto-grader tests on the Gradescope test system. Otter test results should be available in a few minutes after your submission. You should be able to see both PASS / FAIL results for the 20 test cases and your total score, which is accessible via Gradescope Dashboard (as in the image below):

    <img src="images/gradescope.png" width="400">


- **Important:** After you submit, you **need to verify** that your code is visible on Gradescope. If you displayed the output of a large variable (such as `movies`) anywhere in your notebook, **we will not be able to view or grade your submission**. Make sure you don't have any large outputs in any of your cells, and verify after submission that your code can be viewed.

- If you feel you have been incorrectly graded on a particular question during manual review, make a regrade request, and wait for a grader to review your request.
